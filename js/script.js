//www.101Computing.net/connect4-challenge/

var player=1; //1 for Yellow, 2 for Red
var grid = [
  [0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0]
];

//A function used to add a token (when possible) based on the slected column
function selectColumn(col) {
//This function is incomplete.
// ΟΚ The following code should check if the column is not already full and if it is ask the user to chose another column.
// ΟΚ It should also place the token on top of any existing token in the selected column
//It should then check if after placing the token the game continues or if the player has aligned 4 tokens.
 //Finally it should check if the grid is full (Game ends on a draw!) 
  var counter=0;
  for (var i=0; i<6; i++) {
    if (grid[i][col]!=0)
        counter++; 
  }
  if (counter==6) {
      alert('Η στήλη είναι γεμάτη, διάλεξε μια άλλη');
      return;
  }
  var doLoop=true;
  var i=5;
  var line=-1;
  while (doLoop) {
      if (grid[i][col]==0) {
        if (player==1) {
            grid[i][col]=1;
            document.getElementById("colorTurn").innerHTML="Σειρά των κόκκινων";
          } else {
            grid[i][col]=2;
            document.getElementById("colorTurn").innerHTML="Σειρά των κίτρινων";
        }
        line=i;
        doLoop=false;
      } else {
        i--;
        if (i==-1)
            doLoop=false;   
      }
  }
  refreshGrid();
  checkWin(line, col);
  if (isGridFull())
    alert('Δεν υπάρχουν διαθέσιμες θέσεις. Το παιχνίδι έληξε με ισοπαλία!');
  // Next player
  player=(player==1?2:1);
}

function isGridFull() {
    for (var i=0; i<6; i++) {
        for (var j=0; j<7; j++) {
          if (grid[i][j]==0)
            return false;
        }
    } 
    return true;
}

function isArrayValid(checkArray) {
  for (var i=0; i<checkArray.length; i++) {
    row = checkArray[i][0];
    col = checkArray[i][1];
    if (row<0 || row>5)
      return false;
    if (col<0 || col>6)
      return false;
  }
  return true;
}

function checkFour(checkArray) {
  for (var i=0; i<checkArray.length; i++) {
    row = checkArray[i][0]; 
    col = checkArray[i][1];
    if (grid[row][col]!=player)
      return false;
  }
  return true;
}

function checkWin(line, col) {
    // Checking within the same row
    for (c_col=(col-3); c_col<(col+1); c_col++) {
      //console.log("Checking within the same row");
      s_row=[ [line, c_col], [line, c_col+1], [line, c_col+2], [line, c_col+3]];
      if (isArrayValid(s_row)) {
        if (checkFour(s_row)) {
          alert((player==1?"Κέρδισαν τα κίτρινα!":"Κέρδισαν τα κόκκινα"));
          resetGrid();
          return;
        }
      }
    }

    // Checking within the same column
    for (c_row=(line-3); c_row<(line+1); c_row++) {
      //console.log('Checking within the same column');
      s_col=[ [c_row, col], [c_row+1, col], [c_row+2, col], [c_row+3, col] ];
      if (isArrayValid(s_col)) {
        if (checkFour(s_col)) {
          alert((player==1?"Κέρδισαν τα κίτρινα!":"Κέρδισαν τα κόκκινα"));
          resetGrid();
          return;
        }
      }
    }
    
    // Checking main diagonal
    for (w=0; w<4; w++) {
      //console.log('Checking main diagonal');
      s_Array = [ [line-w, col-w], [line-w+1, col-w+1], [line-w+2, col-w+2], [line-w+3, col-w+3] ];
      if (isArrayValid(s_Array)) {
        if (checkFour(s_Array)) {
          alert((player==1?"Κέρδισαν τα κίτρινα!":"Κέρδισαν τα κόκκινα"));
          resetGrid();
          return;
        }
      }
    }

    // Checking inverse diagonal
    for (w=0; w<4; w++) {
      //console.log('Checking inverse diagonal');
      s_Array = [ [line-w, col+w], [line-w+1, col+w-1], [line-w+2, col+w-2], [line-w+3, col+w-3] ];
      if (isArrayValid(s_Array)) {
        if (checkFour(s_Array)) {
          alert((player==1?"Κέρδισαν τα κίτρινα!":"Κέρδισαν τα κόκκινα"));
          resetGrid();
          return;
        }
      }
    }
}

//A function used to refresh the connect4 grid on screen
function refreshGrid() {
  for (var row = 0; row < 6; row++) {
    for (var col = 0; col < 7; col++) {
      if (grid[row][col]==0) { 
                document.getElementById("cell"+row+col).style.backgroundColor="#FFFFFF";
      } else if (grid[row][col]==1) { //1 for yellow
                document.getElementById("cell"+row+col).style.backgroundColor="#FFFF00";
      } else if (grid[row][col]==2) { //1 for yellow
                document.getElementById("cell"+row+col).style.backgroundColor="#FF0000";
       }
    }
  }  
}

function resetGrid() {
  for (var i=0; i<6; i++) {
      for (var j=0; j<7; j++)
        grid[i][j]=0;
  }
  refreshGrid();
}